var dec2 = makeDecline(dqi0, dbfac0, ddec0, dmindec0, dgor0, timeinc0, startdate0, ngshrink0, ngyield0, Rwor0, intWRI, intNRI, taxOilSev, taxGasSev, taxConSev, taxAdVal, drillCompleteCost0, disposalCost0, discountRate0);


var chartConfig = {
  "type": "serial",
  "theme": "none",
  "marginLeft": 70,
  "dataDateFormat": "YYYY-MM-DD",
  "legend":{
    "useGraphSettings":true,
    "position":"right"
  },
  "graphs": [{
    "bullet": "round",
    "bulletBorderAlpha": 1,
    "bulletColor": "#006400",
    "lineColor":"#006400",
    "bulletSize": 5,
    "hideBulletsCount": 50,
    "lineThickness": 2,
    "title": "Oil BOPD",
    "useLineColorForBulletBorder": true,
    "valueField": "rateOil"
  }],
  "chartCursor": {
    "categoryBalloonEnabled":false
  },
  "numberFormatter":{
    "precision":1,
    "decimalSeparator":'.',
    "thousandsSeparator":','
  },
  "categoryField": "timeDT",
  "categoryAxis": {
    "parseDates": true,
    "dashLength": 1,
    "minorGridEnabled": true,
    "labelsEnabled":false,
    "tickLength":0
  },
  "valueAxes": [{
    "ignoreAxisWidth": true,
    "logarithmic":true,
    "title":"Gross Rate"
  }],
  "dataProvider": dec2
};

var chartConfig2 = {
  "type": "serial",
  "theme": "none",
  "marginLeft": 70,
  "pathToImages": "http://www.amcharts.com/lib/3/images/",
  "dataDateFormat": "YYYY-MM-DD",
  "legend":{
    "useGraphSettings":true,
    "position":"right"
  },
  "graphs": [{
    "bullet": "round",
    "bulletBorderAlpha": 1,
    "bulletColor": "#FFFFFF",
    "lineColor":"#bf5700",
    "bulletSize": 5,
    "hideBulletsCount": 50,
    "lineThickness": 2,
    "title": "BTAX Undisc",
    "useLineColorForBulletBorder": true,
    "valueField": "cfUndiscBtax"
  }],
  "chartCursor": {
    "categoryBalloonEnabled":false
  },
  "numberFormatter":{
    "precision":1,
    "decimalSeparator":'.',
    "thousandsSeparator":','
  },
  "categoryField": "timeDT",
  "categoryAxis": {
    "parseDates": true,
    "dashLength": 1,
    "minorGridEnabled": true,
    "labelsEnabled":false,
    "tickLength":0
  },
  "valueAxes": [{
    "ignoreAxisWidth": true,
    "title":"Cash Flow",
    "minimum":0
  }],
  "dataProvider": dec2
};

var chartConfig3 = {
  "type": "serial",
  "theme": "none",
  "marginLeft": 70,
  "pathToImages": "http://www.amcharts.com/lib/3/images/",
  "dataDateFormat": "YYYY-MM-DD",
  "legend":{
    "useGraphSettings":true,
    "position":"right"
  },
  "graphs": [{
    "bullet": "round",
    "bulletBorderAlpha": 1,
    "bulletColor": "#FFFFFF",
    "bulletSize": 5,
    "lineColor":"#006400",
    "hideBulletsCount": 50,
    "lineThickness": 2,
    "title": "Oil Price",
    "useLineColorForBulletBorder": true,
    "valueField": "priceOil"
  }],
  "chartCursor": {},
  "numberFormatter":{
    "precision":1,
    "decimalSeparator":'.',
    "thousandsSeparator":','
  },
  "chartScrollbar": {
    "oppositeAxis":false,
    "offset":30
  },
  "categoryField": "timeDT",
  "categoryAxis": {
    "parseDates": true,
    "dashLength": 1,
    "minorGridEnabled": true
  },
  "valueAxes": [{
    "ignoreAxisWidth": true,
    "title":"Price Deck"
  }],
  "dataProvider": dec2
};

var graph2 = new AmCharts.AmGraph();
graph2.title = "Gas MCFD";
graph2.valueField = "rateGasShrunk";
graph2.bullet = "round";
graph2.bulletBorderColor = "#FFFFFF";
graph2.bulletBorderThickness = 2;
graph2.bulletBorderAlpha = 1;
graph2.lineThickness = 2;
graph2.lineColor = "#ff0000";
graph2.negativeLineColor = "#efcc26";
graph2.hideBulletsCount = 50; // this makes the chart to hide bullets when there are more than 50 series in selection


var graph3 = new AmCharts.AmGraph();
graph3.title = "Water BOPD";
graph3.valueField = "rateWater";
graph3.bullet = "round";
graph3.bulletBorderColor = "0000FF";
graph3.bulletBorderThickness = 2;
graph3.bulletBorderAlpha = 1;
graph3.lineThickness = 2;
graph3.lineColor = "#0000ff";
graph3.negativeLineColor = "#efcc26";
graph3.hideBulletsCount = 50;

var ataxCFgUD = new AmCharts.AmGraph();
ataxCFgUD.title = "ATAX Undisc";
ataxCFgUD.valueField = "cfUndiscAtax";
ataxCFgUD.bullet = "round";
ataxCFgUD.bulletBorderColor = "0000FF";
ataxCFgUD.bulletBorderThickness = 2;
ataxCFgUD.bulletBorderAlpha = 1;
ataxCFgUD.lineThickness = 2;
ataxCFgUD.lineColor = "#890089";
ataxCFgUD.negativeLineColor = "#efcc26";
ataxCFgUD.hideBulletsCount = 50;

var btaxCFg = new AmCharts.AmGraph();
btaxCFg.title = "BTAX Disc";
btaxCFg.valueField = "cfDiscBtax";
btaxCFg.bullet = "round";
btaxCFg.bulletBorderColor = "0000FF";
btaxCFg.bulletBorderThickness = 2;
btaxCFg.bulletBorderAlpha = 1;
btaxCFg.lineThickness = 2;
btaxCFg.lineColor = "#ff00ff";
btaxCFg.negativeLineColor = "#efcc26";
btaxCFg.hideBulletsCount = 50;

var ataxCFg = new AmCharts.AmGraph();
ataxCFg.title = "ATAX Disc";
ataxCFg.valueField = "cfDiscAtax";
ataxCFg.bullet = "round";
ataxCFg.bulletBorderColor = "0000FF";
ataxCFg.bulletBorderThickness = 2;
ataxCFg.bulletBorderAlpha = 1;
ataxCFg.lineThickness = 2;
ataxCFg.lineColor = "#ff7400";
ataxCFg.negativeLineColor = "#efcc26";
ataxCFg.hideBulletsCount = 50;

var priceGasG = new AmCharts.AmGraph();
priceGasG.title = "Gas Price";
priceGasG.valueField = "priceGas";
priceGasG.bullet = "round";
priceGasG.bulletBorderColor = "0000FF";
priceGasG.bulletBorderThickness = 2;
priceGasG.bulletBorderAlpha = 1;
priceGasG.lineThickness = 2;
priceGasG.lineColor = "#ff7400";
priceGasG.negativeLineColor = "#efcc26";
priceGasG.hideBulletsCount = 50;


var charts = [];
charts.push(AmCharts.makeChart("chartdiv", chartConfig));
charts.push(AmCharts.makeChart("chartdiv2", chartConfig2));
charts.push(AmCharts.makeChart("chartdiv3", chartConfig3));
charts[0].addGraph(graph2);
charts[0].addGraph(graph3);
charts[1].addGraph(btaxCFg);
charts[1].addGraph(ataxCFgUD);
charts[1].addGraph(ataxCFg);
charts[2].addGraph(priceGasG);


for (var x in charts) {
  charts[x].addListener("zoomed", syncZoom);
  charts[x].addListener("init", addCursorListeners);
}


function addCursorListeners(event) {
  event.chart.chartCursor.addListener("changed", handleCursorChange);
  event.chart.chartCursor.addListener("onHideCursor", handleHideCursor);
  event.chart.addListener("rendered",zoomChart);
}
function zoomChart() {
    // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
    for (x in charts) {
      charts[x].zoomToDates(startdate0, addDays(startdate0, 365));
    }
}

function syncZoom(event) {
  for (x in charts) {
    if (charts[x].ignoreZoom) {
      charts[x].ignoreZoom = false;
    }
    if (event.chart != charts[x]) {
      charts[x].ignoreZoom = true;
      charts[x].zoomToDates(event.startDate, event.endDate);
    }
  }
}
function handleCursorChange(event) {
  for (var x in charts) {
    if (event.chart != charts[x]) {
      if (event.position) {
        charts[x].chartCursor.isZooming(event.target.zooming);
        charts[x].chartCursor.selectionPosX = event.target.selectionPosX;
        charts[x].chartCursor.forceShow = true;
        charts[x].chartCursor.setPosition(event.position, false, event.target.index);
      }
    }
  }
}

function handleHideCursor() {
  for (var x in charts) {
    if (charts[x].chartCursor.hideCursor) {
      charts[x].chartCursor.forceShow = false;
      charts[x].chartCursor.hideCursor(false);
    }
  }
}

function chartBubbleFormat(category) {
  return "hi";
}
