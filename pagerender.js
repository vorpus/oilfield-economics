$('document').ready(function() {
        $( "#recalc" ).click(function(e) {
          e.preventDefault();
          dqi0 = 1.0*$('#dqi0').val();
          dbfac0 = 1.0*$('#dbfac0').val();
          ddec0 = 1.0*$('#ddec0').val();
          dmindec0 = 1.0*$('#dmindec0').val();
          dgor0 = 1.0*$('#dgor0').val();
          ngshrink0 = 1.0*$('#ngshrink0').val();
          ngyield0 = 1.0*$('#ngyield0').val();
          Rwor0 = 1.0*$('#Rwor0').val();
          intWRI = 0.01*$('#intWRI').val();
          intNRI = 0.01*$('#intNRI').val();
          taxOilSev = 0.01*$('#taxOilSev').val();
          taxGasSev = 0.01*$('#taxGasSev').val();
          taxConSev = taxOilSev;
          taxAdVal = 0.01*$('#taxAdVal').val();
          drillCompleteCost0 = 1000000.0*$('#drillCompleteCost0').val();
          disposalCost0 = 1.0*$('#disposalCost0').val();
          discountRate0 = 0.01*$('#discountRate0').val();
          console.log([dqi0, dbfac0, ddec0, dmindec0, dgor0, timeinc0, startdate0, ngshrink0, ngyield0, Rwor0, intWRI, intNRI, taxOilSev, taxGasSev, taxConSev, taxAdVal, drillCompleteCost0, disposalCost0, discountRate0]);
          var dec2 = makeDecline(dqi0, dbfac0, ddec0, dmindec0, dgor0, timeinc0, startdate0, ngshrink0, ngyield0, Rwor0, intWRI, intNRI, taxOilSev, taxGasSev, taxConSev, taxAdVal, drillCompleteCost0, disposalCost0, discountRate0);
          annIRR = returnAnnualIRR(dec2);
          annNPV = NPV(dec2);
          for (x in charts) {
            charts[x].dataProvider = dec2;
            charts[x].validateData();
          }
          zoomChart();

          $('#econresults').html('BTAX ROR: '+annIRR[0].toFixed(2)+' %'+
                                 '<br>ATAX ROR: '+annIRR[1].toFixed(2)+' %'+
                                 '<br>BTAX NPV: $ '+annNPV[0].formatMoney(2)+
                                 '<br>ATAX NPV: $ '+annNPV[1].formatMoney(2)
                                );

        });

        $('#decline-tab-link').click(function(e) {
          $("#decline-tab").delay(100).fadeIn(100);
          $("#financial-tab").fadeOut(100);
          $('#financial-tab-link').removeClass('active');
          $(this).addClass('active');
          e.preventDefault();
        });
        $('#financial-tab-link').click(function(e) {
          $("#financial-tab").delay(100).fadeIn(100);
          $("#decline-tab").fadeOut(100);
          $('#decline-tab-link').removeClass('active');
          $(this).addClass('active');
          e.preventDefault();
        });
        });